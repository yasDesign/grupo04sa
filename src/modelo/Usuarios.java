/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Usuarios {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String nombre;
    private String apellido;
    private String nacionalidad;
    private String fechanacimiento;
    private String lugarnacimiento;
    private int cedula;
    private String calle;
    private String ciudad;
    private int telefono;
    private String correo;
    private String contrasena;
    
    private String calletrabajo;
    private String ciudadtrabajo;
    private int telefonotrabajo;
    private String correotrabajo;
   
    private String licenciatura;
    private String universidad;
    private int modalidad;
    private String otrosestudios;
    private int tipo;
    private String creado;
    
   
    public Usuarios() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getFechanacimiento() {
        return fechanacimiento;
    }

    public void setFechanacimiento(String fechanacimiento) {
        this.fechanacimiento = fechanacimiento;
    }

    public String getLugarnacimiento() {
        return lugarnacimiento;
    }

    public void setLugarnacimiento(String lugarnacimiento) {
        this.lugarnacimiento = lugarnacimiento;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String getCalletrabajo() {
        return calletrabajo;
    }

    public void setCalletrabajo(String calletrabajo) {
        this.calletrabajo = calletrabajo;
    }

    public String getCiudadtrabajo() {
        return ciudadtrabajo;
    }

    public void setCiudadtrabajo(String ciudadtrabajo) {
        this.ciudadtrabajo = ciudadtrabajo;
    }

    public int getTelefonotrabajo() {
        return telefonotrabajo;
    }

    public void setTelefonotrabajo(int telefonotrabajo) {
        this.telefonotrabajo = telefonotrabajo;
    }

    public String getCorreotrabajo() {
        return correotrabajo;
    }

    public void setCorreotrabajo(String correotrabajo) {
        this.correotrabajo = correotrabajo;
    }

    public String getLicenciatura() {
        return licenciatura;
    }

    public void setLicenciatura(String licenciatura) {
        this.licenciatura = licenciatura;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public int getModalidad() {
        return modalidad;
    }

    public void setModalidad(int modalidad) {
        this.modalidad = modalidad;
    }

    public String getOtrosestudios() {
        return otrosestudios;
    }

    public void setOtrosestudios(String otrosestudios) {
        this.otrosestudios = otrosestudios;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getCreado() {
        return creado;
    }

    public void setCreado(String creado) {
        this.creado = creado;
    }

    public boolean regEstudiante() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.usuario(\n" +
"            nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"            cedula, calle, ciudad, telefono, correo, contrasena, calletrabajo, \n" +
"            ciudadtrabajo, telefonotrabajo, correotrabajo, licenciatura, \n" +
"            universidad, modalidad, otrosestudios)\n" +
"    VALUES ( '"+getNombre()+"','"+getApellido()+"','"+getNacionalidad()+"','"+getFechanacimiento()+"','"+getLugarnacimiento()+"', \n" +
"            "+getCedula()+", '"+getCalle()+"', '"+getCiudad()+"', "+getTelefono()+", '"+getCorreo()+"', '"+getContrasena()+"', '"+getCalletrabajo()+"', \n" +
"            '"+getCiudadtrabajo()+"', "+getTelefonotrabajo()+", '"+getCorreotrabajo()+"', '"+getLicenciatura()+"', \n" +
"            '"+getUniversidad()+"', "+getModalidad()+", '"+getOtrosestudios()+"');";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
     
    public boolean regAdministrador() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.usuario(\n" +
"            nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"            cedula, calle, ciudad, telefono, correo, contrasena,licenciatura, \n" +
"            universidad, modalidad, otrosestudios,tipo)\n" +
"    VALUES ( '"+getNombre()+"','"+getApellido()+"','"+getNacionalidad()+"','"+getFechanacimiento()+"','"+getLugarnacimiento()+"', \n" +
"            "+getCedula()+", '"+getCalle()+"', '"+getCiudad()+"', "+getTelefono()+", '"+getCorreo()+"', '"+getContrasena()+"', \n" +
"            '"+getLicenciatura()+"', '"+getUniversidad()+"', "+getModalidad()+", '"+getOtrosestudios()+"',2);";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }

    public ArrayList<Usuarios> listUsuarios() {
        Statement st;
        ArrayList<Usuarios> l_usuarios=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"       cedula, calle, ciudad, telefono, correo, contrasena, calletrabajo, \n" +
"       ciudadtrabajo, telefonotrabajo, correotrabajo, licenciatura, \n" +
"       universidad, modalidad, otrosestudios, tipo, creado\n" +
"  FROM public.usuario;";
            }else{
                s_sql="SELECT id, nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"       cedula, calle, ciudad, telefono, correo, contrasena, calletrabajo, \n" +
"       ciudadtrabajo, telefonotrabajo, correotrabajo, licenciatura, \n" +
"       universidad, modalidad, otrosestudios, tipo, creado\n" +
"  FROM public.usuario where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Usuarios i_usuario=new Usuarios();
                    i_usuario.setId(r_res.getInt(1));
                    i_usuario.setNombre(r_res.getString(2));
                    i_usuario.setApellido(r_res.getString(3));
                    i_usuario.setNacionalidad(r_res.getString(4));
                    i_usuario.setFechanacimiento(r_res.getString(5));
                    i_usuario.setLugarnacimiento(r_res.getString(6));
                    i_usuario.setCedula(r_res.getInt(7));
                    i_usuario.setCalle(r_res.getString(8));
                    i_usuario.setCiudad(r_res.getString(9));
                    i_usuario.setTelefono(r_res.getInt(10));
                    i_usuario.setCorreo(r_res.getString(11));
                    i_usuario.setContrasena(r_res.getString(12));

                    i_usuario.setCalletrabajo(r_res.getString(13));
                    i_usuario.setCiudadtrabajo(r_res.getString(14));
                    i_usuario.setTelefonotrabajo(r_res.getInt(15));
                    i_usuario.setCorreotrabajo(r_res.getString(16));

                    i_usuario.setLicenciatura(r_res.getString(17));
                    i_usuario.setUniversidad(r_res.getString(18));
                    i_usuario.setModalidad(r_res.getInt(19));
                    i_usuario.setOtrosestudios(r_res.getString(20));

                l_usuarios.add(i_usuario);
            }
            return l_usuarios;
        } catch (SQLException ex) {
            
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return l_usuarios;
        }
    }

    public boolean delUsuario() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.usuario";
            if (getId()>0) {
                s_sql="DELETE FROM public.usuario WHERE id="+getId();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean editUsuario() {
       Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="UPDATE public.usuario\n" +
"   SET nombre='"+getNombre()+"', apellido='"+getApellido()+"', nacionalidad='"+getNacionalidad()+"', fechanacimiento='"+getFechanacimiento()+"', \n" +
"       lugarnacimiento='"+getLugarnacimiento()+"', cedula="+getCedula()+", calle='"+getCalle()+"', ciudad='"+getCiudad()+"', telefono="+getTelefono()+", correo='"+getCorreo()+"', \n" +
"       contrasena='"+getContrasena()+"', calletrabajo='"+getCalletrabajo()+"', ciudadtrabajo='"+getCiudadtrabajo()+"', telefonotrabajo="+getTelefonotrabajo()+", \n" +
"       correotrabajo='"+getCorreotrabajo()+"', licenciatura='"+getLicenciatura()+"', universidad='"+getUniversidad()+"', modalidad="+getModalidad()+", \n" +
"       otrosestudios='"+getOtrosestudios()+"', tipo="+getTipo()+" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    
    
    
}
