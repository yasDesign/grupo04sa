/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josuedjh
 */
public class Requisito {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String nombre;
    private String descripcion;
    
    public Requisito() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public boolean regRequisito() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.requisito(\n" +
"            nombre, descripcion)\n" +
"    VALUES ( '"+getNombre()+"','"+getDescripcion()+"');";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Requisito.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    
    public ArrayList<Requisito> listRequisito() {
        Statement st;
        ArrayList<Requisito> l_requisito=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, nombre, descripcion\n" +
"  FROM public.requisito;";
            }else{
                s_sql="SELECT id, nombre, descripcion\n" +
"  FROM public.requisito where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Requisito i_requisito=new Requisito();
                    i_requisito.setId(r_res.getInt(1));
                    i_requisito.setNombre(r_res.getString(2));
                    i_requisito.setDescripcion(r_res.getString(3));

                l_requisito.add(i_requisito);
            }
            return l_requisito;
        } catch (SQLException ex) {
            
            Logger.getLogger(Requisito.class.getName()).log(Level.SEVERE, null, ex);
            return l_requisito;
        }
    }
    
    public boolean editRequisito() {
       Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="UPDATE public.requisito\n" +
"   SET nombre='"+getNombre()+"', descripcion='"+getDescripcion()+"'\n" +
" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Requisito.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    
    public boolean delRequisito() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.requisito";
            if (getId()>0) {
                s_sql="DELETE FROM public.requisito WHERE id="+getId();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Requisito.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
