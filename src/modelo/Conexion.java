/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Conexion {
    //server virtual
    String URL_DB = "jdbc:postgresql://virtual.fcet.uagrm.edu.bo:5432/";
   // String URL_DB = "jdbc:postgresql://mail.ficct.uagrm.edu.bo:5432/";
    // local
    //String URL_DB = "jdbc:postgresql://localhost:5432/";
    String USER_DB = "grupo02sc";
    String NAME_DB = "db_grupo02sc";
    String PASS_DB = "grupo02grupo02";
    
    private Connection con = null;
    private static Conexion conexion;

    private Conexion()  {
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(URL_DB + NAME_DB, USER_DB, PASS_DB);
            if (con != null) {
                System.out.println("conexion exitosa..!!!");
            } else {
                System.out.println("error en la conexion .!!!");
            }
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public static Conexion getInstancia() {
        if (conexion == null) {
            conexion = new Conexion();
        }
        return conexion;
    }

    public Connection getConexion() {
        return con;
    }
    
    public static void main(String[] args) {
        Conexion mConexion=Conexion.getInstancia();
    }
    
}
