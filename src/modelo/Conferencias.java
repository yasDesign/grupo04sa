/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Conferencias {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String titulo;
    private String descripcion;
    private String disertante;
    private String fecha;
    private String hora;

    
    public Conferencias() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }
        
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDisertante() {
        return disertante;
    }

    public void setDisertante(String disertante) {
        this.disertante = disertante;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    
    public boolean regConferencias() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.conferencias(\n" +
"            titulo, descripcion, disertante, fecha, hora)\n" +
"    VALUES ( '"+getTitulo()+"','"+getDescripcion()+"','"+getDisertante()+"','"+getFecha()+"','"+getHora()+"');";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo insertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Conferencias.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    
    public ArrayList<Conferencias> listConferencias() {
        Statement st;
        ArrayList<Conferencias> l_conferencias=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, titulo, descripcion, disertante, fecha, hora FROM public.conferencias;";
            }else{
                s_sql="SELECT id, titulo, descripcion, disertante, fecha, hora FROM public.conferencias where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Conferencias i_conferencias=new Conferencias();
                    i_conferencias.setId(r_res.getInt(1));
                    i_conferencias.setTitulo(r_res.getString(2));
                    i_conferencias.setDescripcion(r_res.getString(3));
                    i_conferencias.setDisertante(r_res.getString(4));
                    i_conferencias.setFecha(r_res.getString(5));
                    i_conferencias.setHora(r_res.getString(6));

                l_conferencias.add(i_conferencias);
            }
            return l_conferencias;
        } catch (SQLException ex) {
            
            Logger.getLogger(Conferencias.class.getName()).log(Level.SEVERE, null, ex);
            return l_conferencias;
        }
    }

    public boolean editConferencias() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
