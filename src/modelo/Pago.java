/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josuedjh
 */
public class Pago {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String nombre;
    private String descripcion;
    private int cuotainicial;
    private int descuento;
    private int cuotas;
    
    public Pago() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCuotainicial() {
        return cuotainicial;
    }

    public void setCuotainicial(int cuotainicial) {
        this.cuotainicial = cuotainicial;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }
    
    public boolean regPago() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.pago(\n" +
"            nombre, descripcion, cuotainicial, descuento, cuotas)\n" +
"    VALUES ( '"+getNombre()+"','"+getDescripcion()+"','"+getCuotainicial()+"','"+getDescuento()+"','"+getCuotas()+"');";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    
    public ArrayList<Pago> listPago() {
        Statement st;
        ArrayList<Pago> l_pago=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, nombre, descripcion, cuotainicial, descuento, cuotas\n" +
"  FROM public.pago;";
            }else{
                s_sql="SELECT id, nombre, descripcion, cuotainicial, descuento, cuotas\n" +
"  FROM public.pago where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Pago i_pago=new Pago();
                    i_pago.setId(r_res.getInt(1));
                    i_pago.setNombre(r_res.getString(2));
                    i_pago.setDescripcion(r_res.getString(3));
                    i_pago.setCuotainicial(r_res.getInt(4));
                    i_pago.setDescuento(r_res.getInt(5));
                    i_pago.setCuotas(r_res.getInt(6));

                l_pago.add(i_pago);
            }
            return l_pago;
        } catch (SQLException ex) {
            
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, ex);
            return l_pago;
        }
    }
    
    public boolean editPago() {
       Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="UPDATE public.pago\n" +
"   SET nombre='"+getNombre()+"', descripcion='"+getDescripcion()+"', cuotainicial="+getCuotainicial()+", descuento="+getDescuento()+", \n" +
"       cuotas="+getCuotas()+" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    
    public boolean delPago() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.pago";
            if (getId()>0) {
                s_sql="DELETE FROM public.pago WHERE id="+getId();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
