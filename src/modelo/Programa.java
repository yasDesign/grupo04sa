/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josuedjh
 */
public class Programa {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String nombre;
    private String descripcion;
    private int idcapasitacion;
    
    public Programa() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getIdcapasitacion() {
        return idcapasitacion;
    }

    public void setIdcapasitacion(int idcapasitacion) {
        this.idcapasitacion = idcapasitacion;
    }
    
    public boolean regPrograma() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.programa(\n" +
"            nombre, descripcion, idcapasitacion)\n" +
"    VALUES ( '"+getNombre()+"','"+getDescripcion()+"',"+getIdcapasitacion()+");";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    
    public ArrayList<Programa> listPrograma() {
        Statement st;
        ArrayList<Programa> l_programa=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, nombre, descripcion, idcapasitacion\n" +
"  FROM public.programa;";
            }else{
                s_sql="SELECT id, nombre, descripcion, idcapasitacion\n" +
"  FROM public.programa where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Programa i_programa=new Programa();
                    i_programa.setId(r_res.getInt(1));
                    i_programa.setNombre(r_res.getString(2));
                    i_programa.setDescripcion(r_res.getString(3));
                    i_programa.setIdcapasitacion(r_res.getInt(3));

                l_programa.add(i_programa);
            }
            return l_programa;
        } catch (SQLException ex) {
            
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return l_programa;
        }
    }
    
    public boolean editPrograma() {
       Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="UPDATE public.programa\n" +
"   SET nombre='"+getNombre()+"', descripcion='"+getDescripcion()+"', idcapasitacion="+getIdcapasitacion()+"\n" +
" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
    
    public boolean delPrograma() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.programa";
            if (getId()>0) {
                s_sql="DELETE FROM public.programa WHERE id="+getId();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
