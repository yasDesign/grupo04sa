/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josuedjh
 */
public class Horario {
    private Conexion m_Conexion;
    private Connection m_con;
    
   private int id;
   private String dia;
   private String horarioinicial;
   private String horariofinal;
   
   public Horario() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getHorarioinicial() {
        return horarioinicial;
    }

    public void setHorarioinicial(String horarioinicial) {
        this.horarioinicial = horarioinicial;
    }

    public String getHorariofinal() {
        return horariofinal;
    }

    public void setHorariofinal(String horariofinal) {
        this.horariofinal = horariofinal;
    }
   
    public boolean regHorario() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.horario(\n" +
"            dia, horarioinicial, horariofinal)\n" +
"    VALUES ( '"+getDia()+"','"+getHorarioinicial()+"','"+getHorariofinal()+"');";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Horario.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    public ArrayList<Horario> listHorario() {
        Statement st;
        ArrayList<Horario> l_horario=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, dia, horarioinicial, horariofinal" +
"  FROM public.horario;";
            }else{
                s_sql="SELECT id, dia, horarioinicial, horariofinal" +
"  FROM public.horario where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Horario i_horario=new Horario();
                    i_horario.setId(r_res.getInt(1));
                    i_horario.setDia(r_res.getString(2));
                    i_horario.setHorarioinicial(r_res.getString(3));
                    i_horario.setHorariofinal(r_res.getString(4));

                l_horario.add(i_horario);
            }
            return l_horario;
        } catch (SQLException ex) {
            
            Logger.getLogger(Horario.class.getName()).log(Level.SEVERE, null, ex);
            return l_horario;
        }
    }
    
   public boolean editHorario() {
       Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="UPDATE public.horario\n" +
"   SET dia="+getDia()+", horarioinicial='"+getHorarioinicial()+"', horariofinal='"+getHorariofinal()+"'" +
"   WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Pago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } 
    }
   
   public boolean delHorario() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.horario";
            if (getId()>0) {
                s_sql="DELETE FROM public.horario WHERE id="+getId();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Horario.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
