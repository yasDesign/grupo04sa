/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author josuedjh
 */
public class Capacitacionespago {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int idpago;
    private int idcapacitacion;
    private int estado;
    
    public Capacitacionespago() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getIdpago() {
        return idpago;
    }

    public void setIdpago(int idpago) {
        this.idpago = idpago;
    }

    public int getIdcapacitacion() {
        return idcapacitacion;
    }

    public void setIdcapacitacion(int idcapacitacion) {
        this.idcapacitacion = idcapacitacion;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
    
    public boolean regCapacitacionespago() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.capacitacionespago(\n" +
"            idpago, idcapacitacion, estado)\n" +
"    VALUES ( "+getIdpago()+","+getIdcapacitacion()+","+getEstado()+");";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Capacitacionespago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
   
    }
    public boolean delCapacitacionespago() {
        Statement  st;
        try {
            st=m_con.createStatement();
            String s_sql="DELETE FROM public.capacitacionespago";
            if (getIdcapacitacion()>0) {
                s_sql="DELETE FROM public.capacitacionespago WHERE idcapacitacion="+getIdcapacitacion();
            }
            int n_res=st.executeUpdate(s_sql);
            if (n_res==1) {
                return true;
            }
            return false;
        } catch (SQLException ex) {
            Logger.getLogger(Capacitacionespago.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    
}
