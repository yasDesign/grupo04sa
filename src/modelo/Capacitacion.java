/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Capacitacion {
   private Conexion m_Conexion;
   private Connection m_Connection;

   private int id;
   private String aquien;
   private String titulacion;
   private String nombre;
   private int cargaHoraria;
   private float inversion;
   private String fechaInicio;
   private int tipo;
   private int categoria;
   private int gestion;
   
    public Capacitacion() {
        m_Conexion=Conexion.getInstancia();
        m_Connection=m_Conexion.getConexion();
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getCategoria() {
        return categoria;
    }

    public void setCategoria(int categoria) {
        this.categoria = categoria;
    }

    public int getGestion() {
        return gestion;
    }

    public void setGestion(int gestion) {
        this.gestion = gestion;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAquien() {
        return aquien;
    }

    public void setAquien(String aquien) {
        this.aquien = aquien;
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public float getInversion() {
        return inversion;
    }

    public void setInversion(float inversion) {
        this.inversion = inversion;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    
    public boolean regCapacitacion() {
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="INSERT INTO public.capacitaciones(" +
"            aquien, titulacion, nombre, cargahoraria, inversion, fechainicio, " +
"            tipo, categoria, gestion)" +
"    VALUES ( '"+getAquien()+"','"+getTitulacion()+"', '"+getNombre()+"',"+getCargaHoraria()+", "+getInversion()+", '"+getFechaInicio()+"', " +
"            "+getTipo()+", "+getCategoria()+", "+getGestion()+");";
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("insertado exitosao");
                return true;
            }
            System.out.println("no se pudo inseertar en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Capacitacion.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    public boolean editCapacitacion() {
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="UPDATE public.capacitaciones\n" +
"   SET aquien='"+getAquien()+"', titulacion='"+getTitulacion()+"', nombre='"+getNombre()+"', cargahoraria="+getCargaHoraria()+", inversion="+getInversion()+", \n" +
"   fechainicio='"+getFechaInicio()+"', tipo="+getTipo()+", categoria="+getCategoria()+", gestion="+getGestion()+"\n" +
" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("edicion exitosao");
                return true;
            }
            System.out.println("no se pudo edicion en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Capacitacion.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean delCapacitacion() {
    Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="DELETE FROM public.capacitaciones\n" +
" WHERE id="+getId();
            int n_res= st.executeUpdate(s_sql);
            if (n_res==1) {
                System.out.println("eliminacion exitosao");
                return true;
            }
            System.out.println("no se pudo eliminacion en el modelo");
            return false;
        } catch (SQLException ex) {
            
            Logger.getLogger(Capacitacion.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Capacitacion> listCapacitacion() {
        Statement st;
        ArrayList<Capacitacion> l_capacitacion=new ArrayList<>();
        try {
            st=m_Connection.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, aquien, titulacion, nombre, cargahoraria, inversion, fechainicio, \n" +
"       tipo, categoria, gestion\n" +
"  FROM public.capacitaciones;";
            }else{
                s_sql="SELECT id, aquien, titulacion, nombre, cargahoraria, inversion, fechainicio, \n" +
"       tipo, categoria, gestion\n" +
"  FROM public.capacitaciones where id="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Capacitacion i_capacitacion=new Capacitacion();
                    i_capacitacion.setId(r_res.getInt(1));
                    i_capacitacion.setAquien(r_res.getString(2));
                    i_capacitacion.setTitulacion(r_res.getString(3));
                    i_capacitacion.setNombre(r_res.getString(4));
                    i_capacitacion.setCargaHoraria(r_res.getInt(5));
                    i_capacitacion.setInversion(r_res.getFloat(6));
                    i_capacitacion.setFechaInicio(r_res.getString(3));
                    i_capacitacion.setTipo(r_res.getInt(3));
                    i_capacitacion.setCategoria(r_res.getInt(3));
                    i_capacitacion.setGestion(r_res.getInt(3));
                    l_capacitacion.add(i_capacitacion);
            }
            return l_capacitacion;
        } catch (SQLException ex) {
            
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return l_capacitacion;
        }
    }
   
    
}
