/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Inscripcion {
    private Conexion m_Conexion;
    private Connection m_Connection;
   
    private int id;
    private int idusuario;
    private int idadministrador;
    private int idcapacitacion;
    private int idpago;
    private float total;
    
    public Inscripcion() {
        m_Conexion=Conexion.getInstancia();
        m_Connection=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public int getIdusuario() {
        return idusuario;
    }

    public void setIdusuario(int idusuario) {
        this.idusuario = idusuario;
    }

    public int getIdadministrador() {
        return idadministrador;
    }

    public void setIdadministrador(int idadministrador) {
        this.idadministrador = idadministrador;
    }

    public int getIdcapacitacion() {
        return idcapacitacion;
    }

    public void setIdcapacitacion(int idcapacitacion) {
        this.idcapacitacion = idcapacitacion;
    }

    public int getIdpago() {
        return idpago;
    }

    public void setIdpago(int idpago) {
        this.idpago = idpago;
    }

    public boolean regINscripcion() {
          int nro=createInscripcion();
        if (nro==0) {
            return false;
        }
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="INSERT INTO public.detallesinscripcion(\n" +
"            idinscripcion, idcapacitacion, idpago, subtotal)\n" +
"    VALUES ("+nro+", "+getIdcapacitacion()+","+getIdpago()+",(select inversion from capacitacion));";
            st.executeUpdate(s_sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            while(res.next()){
                System.out.println("asd "+res.getInt(1));
                int n_res=res.getInt(1);
                res.close();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;

    }

    private int createInscripcion() {
        Statement st;
        try {
            st=m_Connection.createStatement();
            String s_sql="INSERT INTO public.inscripcion(idusuario, idadministrador) VALUES ("+getIdusuario()+","+getIdadministrador()+");";
            st.executeUpdate(s_sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            res.next();
            int nro=res.getInt(1);
            res.close();
            return nro;
        } catch (SQLException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }
    
    public boolean delInscripcion() {
        return true;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    
    public ArrayList<Inscripcion> listInscripcion() {
        Statement st;
        ArrayList<Inscripcion> l_programa=new ArrayList<>();
        try {
            st=m_Connection.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="SELECT id, idusuario, idadministrador, total\n" +
"  FROM public.inscripcion;";
            }else{
                s_sql="SELECT id, idusuario, idadministrador, total\n" +
"  FROM public.inscripcion where idusuario="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Inscripcion i_programa=new Inscripcion();
                i_programa.setId(r_res.getInt(1));
                i_programa.setIdusuario(r_res.getInt(2));
                i_programa.setIdadministrador(r_res.getInt(3));
                i_programa.setTotal(r_res.getInt(4));
                l_programa.add(i_programa);
            }
            return l_programa;
        } catch (SQLException ex) {
            Logger.getLogger(Programa.class.getName()).log(Level.SEVERE, null, ex);
            return l_programa;
        }
    }

    
}
