/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tecno
 */
public class Mensajes {
    private Conexion m_Conexion;
    private Connection m_con;
    
    private int id;
    private String asunto;
    private String contenido;
    private String fechaCreado;
    private int tipo;
    
    public Mensajes() {
        m_Conexion=Conexion.getInstancia();
        m_con=m_Conexion.getConexion();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getFechaCreado() {
        return fechaCreado;
    }

    public void setFechaCreado(String fechaCreado) {
        this.fechaCreado = fechaCreado;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public boolean sendMensaje() {
        int nro=createMensaje();
        if (nro==0) {
            return false;
        }
        
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.recibir(idusuario, idmensaje) VALUES ("+getId()+","+nro+");";
            st.executeUpdate(s_sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            while(res.next()){
                System.out.println("asd "+res.getInt(1));
                int n_res=res.getInt(1);
                res.close();
                return true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        return false;
    }

    private int createMensaje() {
        Statement st;
        try {
            st=m_con.createStatement();
            String s_sql="INSERT INTO public.mensajes(asunto, contenido, tipo) VALUES ('"+getAsunto()+"','"+getContenido()+"', "+getTipo()+");";
            st.executeUpdate(s_sql,Statement.RETURN_GENERATED_KEYS);
            ResultSet res=st.getGeneratedKeys();
            res.next();
            int nro=res.getInt(1);
            res.close();
            return nro;
        } catch (SQLException ex) {
            Logger.getLogger(Mensajes.class.getName()).log(Level.SEVERE, null, ex);
            return 0;
        }
    }

    public ArrayList<Mensajes> listMensaje() {
        Statement st;
        ArrayList<Mensajes> l_mensaje=new ArrayList<>();
        try {
            st=m_con.createStatement();
            String s_sql="";
            if (getId()==0) {
                s_sql="select idusuario,id,asunto,contenido,tipo  from recibir join mensajes on recibir.idmensaje=mensajes.id";
            }else{
                s_sql="select idusuario,id,asunto,contenido,tipo  from recibir join mensajes on recibir.idmensaje=mensajes.id and recibir.idusuario="+getId();
            }
            ResultSet r_res=st.executeQuery(s_sql);
            while (r_res.next()) {                
                Mensajes i_mensaje=new Mensajes();
                    i_mensaje.setId(r_res.getInt(2));
                    i_mensaje.setAsunto(r_res.getString(3));
                    i_mensaje.setContenido(r_res.getString(4));
                    i_mensaje.setTipo(r_res.getInt(5));
                l_mensaje.add(i_mensaje);
            }
            return l_mensaje;
        } catch (SQLException ex) {
            Logger.getLogger(Usuarios.class.getName()).log(Level.SEVERE, null, ex);
            return l_mensaje;
        }
    }
    /*
    public static void main(String[] args) {
        Mensajes mn=new Mensajes();
        mn.setId(0);
        ArrayList<Mensajes> lis=mn.listMensaje();
        System.out.println("asdas");
    }*/
    
}
