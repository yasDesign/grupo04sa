/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo02sc;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import negocio.NCapacitacion;
import negocio.NCapacitacionespago;
import negocio.NUsuarios;
import negocio.NConferencias;
import negocio.NMensajes;
import negocio.NPago;
import negocio.NHorario;
import negocio.NInscripcion;
import negocio.NRequisito;
import negocio.NPrograma;

/**
 *
 * @author tecno
 */
public class Manejador {

    private int max = 0;
    private PopMessage m_PopMessage2;
    private SMTPMessage m_SMTPMessage2;
    
    private String g_metodo="";
    private String g_origen="";   

    public Manejador() {
        m_PopMessage2 = new PopMessage();
        max = m_PopMessage2.getSize();
        m_PopMessage2.cerrar();
    }

    public void leer() {
        m_PopMessage2 = new PopMessage();
        if (m_PopMessage2.getSize() > max) {
            max++;
            //analizarLineas(m_PopMessage2.getMessageArray(max));
            boolean estado=analizarLineasSi(m_PopMessage2.getMessageArray(max));
            if (estado) {
                enviar_respuesta_(true);
            }else{
                enviar_respuesta_(false);
            }
        }
        m_PopMessage2.cerrar();
    }

    public void analizarLineas(List<String> messageArray) {
        String lineaMetodo = "";
        String lineaUsuario = "";
        int i=0;
        for (String line : messageArray) {
            //System.out.println(line.toString());
            if (line.contains("Return-Path:")) {
              lineaUsuario = line;
            }
            if (line.contains("Subject:")||line.contains("subject:")) {
                if (line.regionMatches(0, "Subject:", 0,8 )||line.regionMatches(0, "subject:", 0,8 )) {
                    while(!messageArray.get(i).contains("]")){
                        lineaMetodo=lineaMetodo+messageArray.get(i);
                        i++;
                    }
                    lineaMetodo=lineaMetodo+messageArray.get(i);
                }                
            }
            i++;
            }
        System.out.println("linea encontrada=>"+lineaMetodo);
            //i++;
        
        //obtener mail usuario
        String mailFrom = getCorreoEmisor(lineaUsuario);
        System.out.println(mailFrom);
        
            //obtener metodo
            //posisiones para metodo y parametros
            int posIni = lineaMetodo.indexOf("[");
            int posFin = lineaMetodo.indexOf("]");
            String metodo = getMetodo(lineaMetodo, posIni);
            System.out.println("metodo-"+metodo);
            //obtener parametros        
            String[] parametros = getParametros(lineaMetodo, posIni, posFin);
            System.out.println(parametros.toString());
            ejecutarMetodos(metodo, parametros, mailFrom);
        

    }

    private String getMetodo(String lineaMetodo, int posIni) {
        //obtener metodo
        String metodo = lineaMetodo.substring(8, posIni).trim();
        metodo=metodo.toUpperCase();
        if (metodo.length() == 0) {
            metodo = "COMANDOS";
        }
        return metodo;
    }

    private String[] getParametros(String lineaMetodo, int posIni, int posFin) {
        String[] parametros = lineaMetodo.substring(posIni + 1, posFin).split(",");
        return parametros;
    }

    private String getCorreoEmisor(String lineaUsuario) {
        //posiciones para usuario mail
        int posIni1 = lineaUsuario.indexOf("<");
        int posFin1 = lineaUsuario.indexOf(">");
        return lineaUsuario.substring(posIni1 + 1, posFin1);
    }
      private void enviarMensajeCorreoOrigen(String prt_mailFrom, String prt_asunto, String prt_mensaje) {
        m_SMTPMessage2 = new SMTPMessage();
        m_SMTPMessage2.sendMessage("grupo02sc@mail.ficct.uagrm.edu.bo", prt_mailFrom, prt_asunto, prt_mensaje);
        m_SMTPMessage2.cerrar();
    }

    private boolean analizarLineasSi(List<String> messageArray) {
        g_origen="";
        g_metodo="";        
        
        String lineaMetodo = "";
        String lineaUsuario = "";
        int i=0;
        for (String line : messageArray) {
            //System.out.println(line.toString());
            if (line.contains("Return-Path:")) {
              lineaUsuario = line;
              //guardar linea correo origen
              g_origen=lineaUsuario;              
            }
            if (line.contains("Subject:")||line.contains("subject:")) {
                if (line.regionMatches(0, "Subject:", 0,8 )||line.regionMatches(0, "subject:", 0,8 )) {
                    while(!messageArray.get(i).contains("]")){
                        lineaMetodo=lineaMetodo+messageArray.get(i);
                        i++;
                    }
                    
                    lineaMetodo=lineaMetodo+messageArray.get(i);
                    //guardar linea metodo globa;
                    
                    g_metodo=lineaMetodo;
                    return true;
                }                
            }
            i++;
            }
        return false;
    }

    private void enviar_respuesta_(boolean b) {
        if (b) {
            String mailFrom = getCorreoEmisor(g_origen);
            
            int posIni = g_metodo.indexOf("[");
            int posFin = g_metodo.indexOf("]");
            String metodo = getMetodo(g_metodo, posIni);
            
            String[] parametros = getParametros(g_metodo, posIni, posFin);
            
            ejecutarMetodos(metodo, parametros, mailFrom);
        }else{
            System.out.println("lo siento no se pudo mandar no se encontro el metodo.. \r\n");
        }
    }

    private void ejecutarMetodos(String prt_asunto, String[] prt_parametros, String prt_mailFrom) {
        String mensaje = "";
        System.out.println(prt_asunto + "...\r\n");

        switch (prt_asunto) {
            
            // GESTIONAR CLIENTES
            case "REGESTUDIANTE":
                NUsuarios m_NUsuarios1=new NUsuarios();
                String s_resUsuario1=m_NUsuarios1.regEstudiante(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resUsuario1));
                break;            
            case "REGADMINISTRADOR":
                NUsuarios m_NUsuarios2=new NUsuarios();
                String s_resUsuario2=m_NUsuarios2.regAdministrador(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resUsuario2));
                break;    
            case "EDITUSUARIO":
                NUsuarios m_NUsuarios3=new NUsuarios();
                String s_resUsuario3=m_NUsuarios3.editUsuario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resUsuario3));
                break;
            case "DELUSUARIO":
                NUsuarios m_NUsuarios4=new NUsuarios();
                String s_resUsuario4=m_NUsuarios4.delUsuario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resUsuario4));
                break;    
            case "LISTUSUARIO":
                System.out.println(prt_asunto + "...\r\n");
                NUsuarios m_NUsuarios5=new NUsuarios();
                String s_resUsuario5=m_NUsuarios5.listUsuario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resUsuario5));
                break;    
            
            // GESTIONAR CONFERENCIAS
            case "REGCONFERENCIAS":
                NConferencias m_NConferencias1=new NConferencias();
                String s_resConferencias1=m_NConferencias1.regConferencias(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resConferencias1));
                break;
            case "LISTCONFERENCIAS":
                System.out.println(prt_asunto + "...\r\n");
                NConferencias m_NConferencias2=new NConferencias();
                String s_resConferencias2=m_NConferencias2.listConferencias(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resConferencias2));
                break; 
                
            // GESTIONAR MENSAJES
            case "SENDMENSAJE":
                System.out.println(prt_asunto+"..");
                NMensajes m_NMensajes1=new NMensajes();
                String s_resMensaje1=m_NMensajes1.sendMensaje(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resMensaje1));
                break;            
            case "LISTMENSAJE":
                System.out.println(prt_asunto+"..");
                NMensajes m_NMensajes2=new NMensajes();
                String s_resMensaje2=m_NMensajes2.listMensaje(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resMensaje2));
                break;  
                
            // GESTIONAR PAGO
            case "REGPAGO":
                NPago m_NPago1=new NPago();
                String s_resPago1=m_NPago1.regPago(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPago1));
                break;
            case "LISTPAGO":
                System.out.println(prt_asunto + "...\r\n");
                NPago m_NPago2=new NPago();
                String s_resPago2=m_NPago2.listPago(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPago2));
                break;
            case "EDITPAGO":
                NPago m_NPago3=new NPago();
                String s_resPago3=m_NPago3.editPago(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPago3));
                break;
            case "DELPAGO":
                NPago m_NPago4=new NPago();
                String s_resPago4=m_NPago4.delPago(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPago4));
                break;
                
                
            // GESTIONAR HORARIO
            case "REGHORARIO":
                NHorario m_NHorario1=new NHorario();
                String s_resHorario1=m_NHorario1.regHorario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resHorario1));
                break;
            case "LISTHORARIO":
                System.out.println(prt_asunto + "...\r\n");
                NHorario m_NHorario2=new NHorario();
                String s_resHorario2=m_NHorario2.listHorario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resHorario2));
                break;
            case "EDITHORARIO":
                NHorario m_NHorario3=new NHorario();
                String s_resHorario3=m_NHorario3.editHorario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resHorario3));
                break;
            case "DELHORARIO":
                NHorario m_NHorario4=new NHorario();
                String s_resHorario4=m_NHorario4.delHorario(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resHorario4));
                break;
            
            // GESTIONAR REQUISITO
            case "REGREQUISITO":
                NRequisito m_NRequisito1=new NRequisito();
                String s_resRequisito1=m_NRequisito1.regRequisito(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resRequisito1));
                break;
            case "LISTREQUISITO":
                System.out.println(prt_asunto + "...\r\n");
                NRequisito m_NRequisito2=new NRequisito();
                String s_resRequisito2=m_NRequisito2.listRequisito(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resRequisito2));
                break;
            case "EDITREQUISITO":
                NRequisito m_NRequisito3=new NRequisito();
                String s_resRequisito3=m_NRequisito3.editRequisito(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resRequisito3));
                break;
            case "DELREQUISITO":
                NRequisito m_NRequisito4=new NRequisito();
                String s_resRequisito4=m_NRequisito4.delRequisito(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resRequisito4));
                break;                
                
            // GESTIONAR CAPACITACION
            case "REGCAPACITACION":
                NCapacitacion m_NCapacitacion1=new NCapacitacion();
                String s_resCapacitacion1=m_NCapacitacion1.regCapacitacion(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resCapacitacion1));
                break;
            case "LISTCAPACITACION":
                System.out.println(prt_asunto + "...\r\n");
                NCapacitacion m_NCapacitacion2=new NCapacitacion();
                String s_resCapacitacion2=m_NCapacitacion2.listCapacitacion(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resCapacitacion2));
                break;
            case "EDITCAPACITACION":
                NCapacitacion m_NCapacitacion3=new NCapacitacion();
                String s_resCapacitacion3=m_NCapacitacion3.editCapacitacion(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resCapacitacion3));
                break;
            case "DELCAPACITACION":
                NCapacitacion m_NCapacitacion4=new NCapacitacion();
                String s_resCapacitacion4=m_NCapacitacion4.delCapacitacion(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resCapacitacion4));
                break;                
                           
            // GESTIONAR PROGRAMA
            case "REGPROGRAMA":
                NPrograma m_NPrograma1=new NPrograma();
                String s_resPrograma1=m_NPrograma1.regPrograma(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPrograma1));
                break;
            case "LISTPROGRAMA":
                System.out.println(prt_asunto + "...\r\n");
                NPrograma m_NPrograma2=new NPrograma();
                String s_resPrograma2=m_NPrograma2.listPrograma(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPrograma2));
                break;
            case "EDITPROGRAMA":
                NPrograma m_NPrograma3=new NPrograma();
                String s_resPrograma3=m_NPrograma3.editPrograma(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPrograma3));
                break;
            case "DELPROGRAMA":
                NPrograma m_NPrograma4=new NPrograma();
                String s_resPrograma4=m_NPrograma4.delPrograma(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resPrograma4));
                break;                
                   
            // GESTIONAR Capacitacionespago
            case "REGCAPAPAGO":
                NCapacitacionespago m_NCapacitacionespago1=new NCapacitacionespago();
                String s_resm_NCapacitacionespago1=m_NCapacitacionespago1.regCapacitacionespago(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resm_NCapacitacionespago1));
                break;
            case "DELCAPAPAGO":
                NCapacitacionespago m_NCapacitacionespago4=new NCapacitacionespago();
                String s_resCapacitacionespago4=m_NCapacitacionespago4.delRequisito(prt_parametros);
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeRespuesta(s_resCapacitacionespago4));
                break;                 
                
                // AYUDA
            case "COMANDOS":
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto,getMensajeAyuda());
                break;            

            default:
                System.out.println(prt_asunto + " no existe ...\r\n");
                String mensaje5 = "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>en la instruccion porfavor revisa  al enviado COMANDOS[]; de la aplicacion</p></div>";
                enviarMensajeCorreoOrigen(prt_mailFrom, prt_asunto, getMensajeRespuesta(mensaje5));
                break;
                
        }
    }
    
    //******************************//
    //***ESTILO PARA LA VISTA***/////
    //******************************//
    public String getMensajeAyuda(){
        
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/NENBOm.css'>";
        String titulo="<div>\n" +
"  <h2>Comandos de la aplicacion \"CAPACITACION CON INEGAS\"</h2>\n" +
"</div>";
        String ayudaUsuarios="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE USUARIOS</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR USUARIO ESTUDIANTE :</strong>\n" +
"  <p>RegEstudiante[nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"            cedula, calle, ciudad, telefono, correo, contrasena, calletrabajo, \n" +
"            ciudadtrabajo, telefonotrabajo, correotrabajo, licenciatura, \n" +
"            universidad, modalidad, otrosestudios];</p>\n" +
"  <strong>EJEMPLO DE REGISTRAR USUARIO ESTUDIANTE :</strong>\n" +
"  <p>RegEstudiante[marcos,lopez,bolivia,10-04-1991,yapacani,7545,las palmas,santa cruz,784512,mario@live.com,admin12,primer anillo,santa cruz,784512, dve@live.com,suistema,umss,1,sistema];</p>\n" +
                
"  <strong>REGISTRAR USUARIO ADMINISTRATIVO :</strong> \n" +
"  <p>RegAdministrador[\n" +
"            nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"            cedula, calle, ciudad, telefono, correo, contrasena, licenciatura, \n" +
"            universidad, modalidad, otrosestudios, tipo];</p>\n" +
"  <strong>EJEPLO DE REGISTRAR USUARIO ADMINISTRATIVO :</strong> \n" +
                ""+
"  <p>RegAdministrador[laura,perez,bolivia, 12-10-1991,yapacani,4,las flores,yapacani,45,laura@live.com,admin123,comercial,upds,2,diseno,2];</p>\n" +
"   <strong>EDITAR USUARIO :</strong>\n" +
"  <p>EditUsuario[id, nombre, apellido, nacionalidad, fechanacimiento, lugarnacimiento, \n" +
"            cedula, calle, ciudad, telefono, correo, contrasena, calletrabajo, \n" +
"            ciudadtrabajo, telefonotrabajo, correotrabajo, licenciatura, \n" +
"            universidad, modalidad, otrosestudios, tipo];</p>\n"+ 
"  <strong>ELIMINAR USUARIO :</strong>\n" +
"  <p>DelUsuario[id o 0];</p>\n" +
"  <strong>LISTAR USUARIOS :</strong>\n" +
"  <p>listar todo los usurios = 0</p>\n" +
"  <p>listar un usuario especifico = 1</p>\n" +

"  <p>ListUsuario[id o 0]</p>\n" +
         "  <p>ejemplo ListUsuario[0]</p>\n" +
"  \n" +
                
                
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todos los usuarios.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara a todos los usuarios sin importar el tipo.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
     String ayudaConferencia=""
             + "<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE CONFERENCIA</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR UNA CONFERENCIA :</strong>\n" +
"  <p>REGCONFERENCIAS[titulo, descripcion, disertante, fecha, hora];</p>\n" +
"  <strong>EJEMPLO INSERTAR CONFERENCIA :</strong> \n" +
"  <p>REGCONFERENCIAS[bots,usa bots en negocio,mario perez,10-10-2018,11:30]</p>\n" +
"  <strong>ELIMINAR USUARIO :</strong>\n" +
"  <p>DELCONFERENCIAS[id];</p>\n" +
"   <strong>EJEMPLO ELIMINAR USUARIO :</strong>\n" +
"  <p>DELCONFERENCIAS[2];</p>\n" +
"    <p>DELCONFERENCIAS[0];</p>\n" +
"  <strong>LISTAR CONFERENCIAS :</strong>\n" +
"  <p>LISTCONFERENCIAS[id o 0]</p>\n" +
"\n" +
"    <p>LISTCONFERENCIAS[0]</p>\n" +
"\n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todos las conferencia.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara a todos las conferencia sin importar el tipo.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";   
     String ayudaMensajes="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE MENSAJES</h3>\n" +
"  </div>\n" +
"  <strong>ENVIAR MENSAJES :</strong>\n" +
"  <p>sendMensaje[idusuario o 0,asunto,contenido,tipo];</p>\n" +
"  <strong>LISTAR MENSAJES</strong> \n" +
" <p>listMensaje[idusuario,0];</p>\n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todos los mensjaes del usuario.\n" +
"    </p class=\"recomendacion\">\n" +
"    <strong>enviar =></strong> si introduces 0 se enviara a todos los usuarios.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
     
     String ayudaAConferencia="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE CONFERENCIAS</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRO A CONFERENCIA :</strong>\n" +
"  <p>RegConferencia[];</p>\n" +
"  <strong>ELIMINAR CONFERENCIA :</strong>\n" +
"  <p>DelConferencia[];</p>\n" +
"  <strong>EDITAR CONFERENCIA :</strong>\n" +
"  <p>editConferencia[]</p>\n" +
"  <strong>LISTAR CONFERENCIA :</strong>\n" +
"  <p>ListConferencia[]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todas las conferencias.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara a todos los usuarios sin importar el tipo.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
    String ayudaRequisitos="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE REQUISITOS</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR REQUISITO:</strong>\n" +
"  <p>RegRequisito[nombre,descripcion];</p>\n" +
"  <strong>ELIMINAR REQUISITO :</strong>\n" +
"  <p>DelRequisito[0 o id];</p>\n" +
"  <strong>EDITAR REQUISITO :</strong>\n" +
"  <p>editRequisito[id,nombre,descripcion]</p>\n" +
"  <strong>LISTAR REQUISITO :</strong>\n" +
"  <p>ListRequisito[id o 0]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todas los requisitos.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara todos los requisitos, si introduce id se eliminara solo el especifico.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
    String ayudaHorario="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE HORARIOS</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR HORARIO:</strong>\n" +
"  <p>RegHorario[dia,horaInicio,horaFin];</p>\n" +
"  <strong>ELIMINAR HORARIO :</strong>\n" +
"  <p>DelHorario[0 o id];</p>\n" +
"  <strong>EDITAR HORARIO :</strong>\n" +
"  <p>editHorario[id,dia,horaInicio,horaFin]</p>\n" +
"  <strong>LISTAR HORARIO :</strong>\n" +
"  <p>ListHorario[id o 0]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todas los horarios.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara todos los horarios, si introduce id se eliminara solo el especifico.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
    
    String ayudaPagos="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE PAGOS</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR PAGOS:</strong>\n" +
"  <p>RegPagos[nombre,descripcion,cuotaInicial,descuento,nros cuotas];</p>\n" +
"  <strong>ELIMINAR HORARIO :</strong>\n" +
"  <p>DelPago[0 o id];</p>\n" +
"  <strong>EDITAR HORARIO :</strong>\n" +
"  <p>EditPago[id,nombre,descripcion,cuotaInicial,descuento,nros cuotas]</p>\n" +
"  <strong>LISTAR HORARIO :</strong>\n" +
"  <p>ListPago[id o 0]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara todos los pagos.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara todos los pagos, si introduce id se eliminara solo el especifico.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";
    
    String ayudaCapacitacion="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE CAPACITACION</h3>\n" +
"  </div>\n" +
"  <strong>REGISTRAR CAPACITACION :</strong>\n" +
"  <p>RegCapacitacion[aquien, titulacion, nombre, cargahoraria, inversion, fechainicio, \n" +
"            tipo, categoria, gestion];</p>\n" +
"  <strong>EDITAR CAPACITACION :</strong>\n" +
"  <p>EditCapacitacion[id,aquien, titulacion, nombre, cargahoraria, inversion, fechainicio, \n" +
"            tipo, categoria, gestion];</p>\n" +
"  <strong>ELIMINAR CAPACITACION :</strong>\n" +
"  <p>DelCapacitacion[id o 0];</p>\n" +
"  <strong>LISTAR CAPACITACIONES:</strong>\n" +
"  <p>ListCapacitacion[id o 0]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara todas las capacitaciones.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara a todos las capacitaciones sin importar el tipo.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";

String ayudaInscripciones="<div class=\"box\">\n" +
"  <div class=\"box-title\">\n" +
"    <h3>COMANDOS DE INSCRIPCION A  LAS CAPACITACIONES</h3>\n" +
"  </div>\n" +
"  <strong>INSCRIPCION:</strong>\n" +
"  <p>RegInscripcion[idusuario,idadministrador,idcapacitacion,idpago];\n" +
" </p>\n" +
"  <strong>LISTAR Inscripciones :</strong>\n" +
"  <p>ListUsuario[id o 0]</p>\n" +
"  <strong>Eliminar Inscripciones :</strong>\n" +
"  <p>DelUsuario[id o 0]</p>\n" +
"  \n" +
"  <div class=\"box-title\">\n" +
"    <h4>RECOMENDACION:</h4>\n" +
"    <p class=\"recomendacion\"><strong>Listar =></strong> si introduces 0 se listara a todos los usuarios.\n" +
"    </p>\n" +
"    <p class=\"recomendacion\">\n" +
"    <strong>Eliminar =></strong> si introduces 0 se eliminara a todos los usuarios sin importar el tipo.  \n" +
"    </p>\n" +
"  </div>\n" +
"</div>";    
    
        return "Content-Type:text/html;\r\n<html>"
                +estilo               
                +titulo
                +ayudaUsuarios
                +ayudaConferencia
                +ayudaMensajes
                +ayudaAConferencia
                +ayudaRequisitos
                +ayudaHorario
                +ayudaPagos
                +ayudaCapacitacion  
                +ayudaInscripciones
                + "</html>";
        
    }
    
    public String getMensajeTabla(String res){
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/NENBOm.css'>";
        return "Content-Type:text/html;\r\n<html>"+estilo+res+"</html>";
                
    }
 
    public String getMensajeRespuesta(String res){
        
        String estilo="<link rel='stylesheet' href='https://codepen.io/ingyas/pen/NENBOm.css'>";
        return "Content-Type:text/html;\r\n<html>"+estilo+res+"</html>";
    }

    
}
