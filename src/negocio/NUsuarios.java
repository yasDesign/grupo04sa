/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import modelo.Usuarios;

/**
 *
 * @author tecno
 */
public class NUsuarios {

    private Usuarios m_usuario;

    public NUsuarios() {
        m_usuario = new Usuarios();
    }

    public String regEstudiante(String[] prt_parametros) {
//        if (validarParaRegEstudiante(prt_parametros)) {
            m_usuario.setNombre(prt_parametros[0]);
            m_usuario.setApellido(prt_parametros[1]);
            m_usuario.setNacionalidad(prt_parametros[2]);
            m_usuario.setFechanacimiento(prt_parametros[3]);
            m_usuario.setLugarnacimiento(prt_parametros[4]);
            m_usuario.setCedula(Integer.parseInt(prt_parametros[5]));
            m_usuario.setCalle(prt_parametros[6]);
            m_usuario.setCiudad(prt_parametros[7]);
            m_usuario.setTelefono(Integer.parseInt(prt_parametros[8]));
            m_usuario.setCorreo(prt_parametros[9]);
            m_usuario.setContrasena(prt_parametros[10]);
            
            m_usuario.setCalletrabajo(prt_parametros[11]);
            m_usuario.setCiudadtrabajo(prt_parametros[12]);
            m_usuario.setTelefonotrabajo(Integer.parseInt(prt_parametros[13]));
            m_usuario.setCorreotrabajo(prt_parametros[14]);
            
            m_usuario.setLicenciatura(prt_parametros[15]);
            m_usuario.setUniversidad(prt_parametros[16]);
            m_usuario.setModalidad(Integer.parseInt(prt_parametros[17]));
            m_usuario.setOtrosestudios(prt_parametros[18]);
            
            boolean b_res=m_usuario.regEstudiante();
            if (b_res) {
                return mensajeCorrecto("registro de usuario estudiante correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
  //      }
  //      return mensajeError("revisar los parametros incorrectos");
   }

    public String regAdministrador(String[] prt_parametros) {
        //if (validarParaRegAdministrador(prt_parametros)) {
            m_usuario.setNombre(prt_parametros[0]);
            m_usuario.setApellido(prt_parametros[1]);
            m_usuario.setNacionalidad(prt_parametros[2]);
            m_usuario.setFechanacimiento(prt_parametros[3]);
            m_usuario.setLugarnacimiento(prt_parametros[4]);
            m_usuario.setCedula(Integer.parseInt(prt_parametros[5]));
            m_usuario.setCalle(prt_parametros[6]);
            m_usuario.setCiudad(prt_parametros[7]);
            m_usuario.setTelefono(Integer.parseInt(prt_parametros[8]));
            m_usuario.setCorreo(prt_parametros[9]);
            m_usuario.setContrasena(prt_parametros[10]);
            
            m_usuario.setLicenciatura(prt_parametros[11]);
            m_usuario.setUniversidad(prt_parametros[12]);
            m_usuario.setModalidad(Integer.parseInt(prt_parametros[13]));
            m_usuario.setOtrosestudios(prt_parametros[14]);
           
            boolean b_res=m_usuario.regAdministrador();
            if (b_res) {
                return mensajeCorrecto("registro de usuario administrativo correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
       // }
       // return mensajeError("revisar los parametros incorrectos");
    
    }

    public String editUsuario(String[] prt_parametros) {
         //if (validarParaEditUsuario(prt_parametros)) {
            m_usuario.setId(Integer.parseInt(prt_parametros[0]));
            m_usuario.setNombre(prt_parametros[1]);
            m_usuario.setApellido(prt_parametros[2]);
            m_usuario.setNacionalidad(prt_parametros[3]);
            m_usuario.setFechanacimiento(prt_parametros[4]);
            m_usuario.setLugarnacimiento(prt_parametros[5]);
            m_usuario.setCedula(Integer.parseInt(prt_parametros[6]));
            m_usuario.setCalle(prt_parametros[7]);
            m_usuario.setCiudad(prt_parametros[8]);
            m_usuario.setTelefono(Integer.parseInt(prt_parametros[9]));
            m_usuario.setCorreo(prt_parametros[10]);
            m_usuario.setContrasena(prt_parametros[11]);
            
            m_usuario.setCalletrabajo(prt_parametros[12]);
            m_usuario.setCiudadtrabajo(prt_parametros[13]);
            m_usuario.setTelefonotrabajo(Integer.parseInt(prt_parametros[14]));
            m_usuario.setCorreotrabajo(prt_parametros[15]);
            
            m_usuario.setLicenciatura(prt_parametros[16]);
            m_usuario.setUniversidad(prt_parametros[17]);
            m_usuario.setModalidad(Integer.parseInt(prt_parametros[18]));
            m_usuario.setOtrosestudios(prt_parametros[19]);
            m_usuario.setTipo(Integer.getInteger(prt_parametros[20]));
            
            boolean b_res=m_usuario.editUsuario();
            if (b_res) {
                return mensajeCorrecto("edicion de usuario correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
       // }
       // return mensajeError("revisar los parametros incorrectos");
        
    }

    public String delUsuario(String[] prt_parametros) {
       // if (validarParaDelUsuario(prt_parametros)) {
            m_usuario.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_usuario.delUsuario();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de usuario realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        //}
       // return mensajeError("revisar los parametros incorrectos");
    }

    public String listUsuario(String[] prt_parametros) {
        ArrayList<Usuarios> l_usuarios=new ArrayList<>();
        if (validarParaListUsuario(prt_parametros)) {
            m_usuario.setId(Integer.parseInt(prt_parametros[0]));
            l_usuarios=m_usuario.listUsuarios();
        if (l_usuarios.size()>0) {
            String s_res="<h2>Lista de usuarios de INEGAS</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>nombre</td>"
                    + "<td>apellido</td>"
                    + "<td>correo</td>"
                    + "<td>universidad</td>"
                    + "</tr>";
            for (Usuarios i_usuario : l_usuarios) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_usuario.getId()+"</td>";
                s_res=s_res+"<td>"+i_usuario.getNombre()+"</td>";
                s_res=s_res+"<td>"+i_usuario.getApellido()+"</td>";
                s_res=s_res+"<td>"+i_usuario.getCorreo()+"</td>";  
                s_res=s_res+"<td>"+i_usuario.getUniversidad()+"</td>";  
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }

    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private boolean validarParaRegAdministrador(String[] prt_parametros) {
        if (prt_parametros.length!=15) {
            return false;
        }
        if (!esNumero(prt_parametros[5])||!esNumero(prt_parametros[8])||!esNumero(prt_parametros[13])) {
            return false;
        }
        if (!esCorreo(prt_parametros[9])) {
            return false;
        }
        return true;
    }

    private boolean validarParaListUsuario(String[] prt_parametros) {
        if (prt_parametros.length!=1) {
            return false;
        }
        if (!esNumero(prt_parametros[0])) {
            return false;
        }
        return true;
    }

    private boolean validarParaEditUsuario(String[] prt_parametros) {
        if (prt_parametros.length!=21) {
            return false;
        }
        if (!esNumero(prt_parametros[0])||!esNumero(prt_parametros[6])||!esNumero(prt_parametros[9])||!esNumero(prt_parametros[18])||!esNumero(prt_parametros[20])) {
            return false;
        }
        if (!esCorreo(prt_parametros[10])) {
            return false;
        }
        return true;
    }

    private boolean validarParaDelUsuario(String[] prt_parametros) {
        if (prt_parametros.length!=1) {
            return false;
        }
        if (!esNumero(prt_parametros[1])) {
            return false;
        }
        return true;
    }    
    
    private boolean validarParaRegEstudiante(String[] prt_parametros) {
       /* if (prt_parametros.length!=19) {
            return false;
        }*/
        if (!esNumero(prt_parametros[0])||!esNumero(prt_parametros[6])||!esNumero(prt_parametros[9])||!esNumero(prt_parametros[13])||!esNumero(prt_parametros[17])) {
           return false;
        }
        if (!esCorreo(prt_parametros[9])||!esCorreo(prt_parametros[9])) {
            return false;
        }
        return true;
    }

    private boolean esNumero(String prt_parametro) {
        try {
            Integer.parseInt(prt_parametro);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private boolean esCorreo(String prt_parametro) {
        String emailPatron = "^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailPatron);
        Matcher matcher = pattern.matcher(prt_parametro);
        if (matcher.matches()) {
            System.out.println("correo valido");
            return true;
        } else {
            System.out.println("correo no validao");
            return false;
        }
    }

    
    
}
