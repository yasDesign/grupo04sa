/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
import java.util.ArrayList;
import modelo.Conferencias;

/**
 *
 * @author josuedjh
 */
public class NConferencias {
    
    private Conferencias m_conferencias;

    public NConferencias() {
        m_conferencias = new Conferencias();
    }
    
    public String regConferencias(String[] prt_parametros) {
        if (validarParaRegConferencias(prt_parametros)) {
            m_conferencias.setTitulo(prt_parametros[0]);
            m_conferencias.setDescripcion(prt_parametros[1]);
            m_conferencias.setDisertante(prt_parametros[2]);
            m_conferencias.setFecha(prt_parametros[3]);
            m_conferencias.setHora(prt_parametros[4]);
            
            boolean b_res=m_conferencias.regConferencias();
            if (b_res) {
                return mensajeCorrecto("registro de Conferencias correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    
    public String listConferencias(String[] prt_parametros) {
        ArrayList<Conferencias> l_conferencias=new ArrayList<>();
        if (validarParaListConferencias(prt_parametros)) {
            m_conferencias.setId(Integer.parseInt(prt_parametros[0]));
            l_conferencias=m_conferencias.listConferencias();
        if (l_conferencias.size()>0) {
            String s_res="<h2>Lista de Conferencias de INEGAS</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>titulo</td>"
                    + "<td>descripcion</td>"
                    + "<td>disertante</td>"
                    + "<td>fecha</td>"
                    + "<td>hora</td>"
                    + "</tr>";
            for (Conferencias i_conferencias : l_conferencias) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_conferencias.getId()+"</td>";
                s_res=s_res+"<td>"+i_conferencias.getTitulo()+"</td>";
                s_res=s_res+"<td>"+i_conferencias.getDescripcion()+"</td>";
                s_res=s_res+"<td>"+i_conferencias.getDisertante()+"</td>";  
                s_res=s_res+"<td>"+i_conferencias.getFecha()+"</td>";
                s_res=s_res+"<td>"+i_conferencias.getHora()+"</td>";  
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }
    
    
    public String editConferencias(String[] prt_parametros){
         if (validarParaRegConferencias(prt_parametros)) {
            m_conferencias.setId(Integer.parseInt(prt_parametros[0]));
            m_conferencias.setTitulo(prt_parametros[0]); 
            m_conferencias.setDescripcion(prt_parametros[1]);
            m_conferencias.setDisertante(prt_parametros[2]);
            m_conferencias.setFecha(prt_parametros[3]);
            m_conferencias.setHora(prt_parametros[4]);
            
            boolean b_res=m_conferencias.editConferencias();
            if (b_res) {
                return mensajeCorrecto("edita de Conferencias correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    private boolean validarParaRegConferencias(String[] prt_parametros) {
        return true;
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaListConferencias(String[] prt_parametros) {
        return true;
    }
    
}
