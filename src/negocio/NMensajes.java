/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Mensajes;

/**
 *
 * @author tecno
 */
public class NMensajes {
    private Mensajes m_Mensajes;
    
    public NMensajes() {
        m_Mensajes=new Mensajes();
    }
    
    public String sendMensaje(String[] prt_parametros) {
        m_Mensajes.setId(Integer.parseInt(prt_parametros[0]));
        m_Mensajes.setAsunto(prt_parametros[1]);
        m_Mensajes.setContenido(prt_parametros[2]);
        m_Mensajes.setTipo(Integer.parseInt(prt_parametros[3]));
        boolean b_res=m_Mensajes.sendMensaje();
        if (b_res) {
            return mensajeCorrecto("mensaje enviado correctamentamente");
        }
        return mensajeError("parametros incorrectos");
    }

    public String listMensaje(String[] prt_parametros) {
        m_Mensajes.setId(Integer.parseInt(prt_parametros[0]));
        ArrayList<Mensajes> l_mensajes=m_Mensajes.listMensaje();
        if (l_mensajes.size()>0) {
            String s_res="<h2>Lista de mensajes</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>asunto</td>"
                    + "<td>contenido</td>"
                    + "<td>tipo</td>"
                    + "</tr>";
            for (Mensajes i_mensaje : l_mensajes) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_mensaje.getId()+"</td>";
                s_res=s_res+"<td>"+i_mensaje.getAsunto()+"</td>";
                s_res=s_res+"<td>"+i_mensaje.getContenido()+"</td>";  
                s_res=s_res+"<td>"+i_mensaje.getTipo()+"</td>";  
                s_res=s_res+"</tr>";
            }
            s_res=s_res+"</table>";
            return s_res;
            
        }
        return mensajeCorrecto("lista vacia");
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
}
