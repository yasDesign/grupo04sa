/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
import java.util.ArrayList;
import modelo.Pago;

/**
 *
 * @author josuedjh
 */
public class NPago {
    private Pago m_pago;

    public NPago() {
        m_pago = new Pago();
    }
    
    public String regPago(String[] prt_parametros) {
        if (validarParaRegPago(prt_parametros)) {
            m_pago.setNombre(prt_parametros[0]);
            m_pago.setDescripcion(prt_parametros[1]);
            m_pago.setCuotainicial(Integer.parseInt(prt_parametros[2]));
            m_pago.setDescuento(Integer.parseInt(prt_parametros[3]));
            m_pago.setCuotas(Integer.parseInt(prt_parametros[4]));
            
            boolean b_res=m_pago.regPago();
            if (b_res) {
                return mensajeCorrecto("registro de usuario estudiante correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaRegPago(String[] prt_parametros) {
        return true;
    }
    
    public String listPago(String[] prt_parametros) {
        ArrayList<Pago> l_pago=new ArrayList<>();
        if (validarParaListPago(prt_parametros)) {
            m_pago.setId(Integer.parseInt(prt_parametros[0]));
            l_pago=m_pago.listPago();
        if (l_pago.size()>0) {
            String s_res="<h2>Lista de pago</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>nombre</td>"
                    + "<td>descripcion</td>"
                    + "<td>couta inicial</td>"
                    + "<td>descuento</td>"
                    + "<td>couta</td>"
                    + "</tr>";
            for (Pago i_pago : l_pago) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_pago.getId()+"</td>";
                s_res=s_res+"<td>"+i_pago.getNombre()+"</td>";
                s_res=s_res+"<td>"+i_pago.getDescripcion()+"</td>";
                s_res=s_res+"<td>"+i_pago.getCuotainicial()+"</td>";  
                s_res=s_res+"<td>"+i_pago.getDescuento()+"</td>";
                s_res=s_res+"<td>"+i_pago.getCuotas()+"</td>";  
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }
        private boolean validarParaListPago(String[] prt_parametros) {
        return true;
    }
        
    public String editPago(String[] prt_parametros) {
         if (validarParaEditPago(prt_parametros)) {
            m_pago.setId(Integer.parseInt(prt_parametros[0]));
            m_pago.setNombre(prt_parametros[1]);
            m_pago.setDescripcion(prt_parametros[2]);
            m_pago.setCuotainicial(Integer.parseInt(prt_parametros[3]));
            m_pago.setDescuento(Integer.parseInt(prt_parametros[4]));
            m_pago.setCuotas(Integer.parseInt(prt_parametros[5]));
          
            boolean b_res=m_pago.editPago();
            if (b_res) {
                return mensajeCorrecto("edicion de pagos correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
        
    }
    private boolean validarParaEditPago(String[] prt_parametros) {
        return true;
    }

    
    public String delPago(String[] prt_parametros) {
        if (validarParaDelPago(prt_parametros)) {
            m_pago.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_pago.delPago();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de pago realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private boolean validarParaDelPago(String[] prt_parametros) {
        return true;
    }  
    
}
