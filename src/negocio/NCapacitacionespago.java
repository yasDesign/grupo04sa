/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
/**
 *
 * @author josuedjh
 */
import modelo.Capacitacionespago;
public class NCapacitacionespago {
      private Capacitacionespago m_capacitacionespago;

    public NCapacitacionespago() {
        m_capacitacionespago = new Capacitacionespago();
    }
    
  public String regCapacitacionespago(String[] prt_parametros) {
        if (validarParaRegCapacitacionespago(prt_parametros)) {
            m_capacitacionespago.setIdpago(Integer.parseInt(prt_parametros[0]));
            m_capacitacionespago.setIdcapacitacion(Integer.parseInt(prt_parametros[1]));
            m_capacitacionespago.setEstado(Integer.parseInt(prt_parametros[2]));


            boolean b_res=m_capacitacionespago.regCapacitacionespago();
            if (b_res) {
                return mensajeCorrecto("registro capacitaciones de pago es correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaRegCapacitacionespago(String[] prt_parametros) {
        return true;
    }
    public String delRequisito(String[] prt_parametros) {
        if (validarParaDelCapacitacionespago(prt_parametros)) {
            m_capacitacionespago.setIdcapacitacion(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_capacitacionespago.delCapacitacionespago();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de Capacitaciones de pago realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private boolean validarParaDelCapacitacionespago(String[] prt_parametros) {
        return true;
    }
}
