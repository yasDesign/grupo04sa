/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Requisito;

/**
 *
 * @author josuedjh
 */
public class NRequisito {
    private Requisito m_requisito;

    public NRequisito() {
        m_requisito = new Requisito();
    }
    
   public String regRequisito(String[] prt_parametros) {
        if (validarParaRegRequisito(prt_parametros)) {
            m_requisito.setNombre(prt_parametros[0]);
            m_requisito.setDescripcion(prt_parametros[1]);
            
            boolean b_res=m_requisito.regRequisito();
            if (b_res) {
                return mensajeCorrecto("registro de requisitos es correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaRegRequisito(String[] prt_parametros) {
        return true;
    }
    
    public String listRequisito(String[] prt_parametros) {
        ArrayList<Requisito> l_requisito=new ArrayList<>();
        if (validarParaListRequisito(prt_parametros)) {
            m_requisito.setId(Integer.parseInt(prt_parametros[0]));
            l_requisito=m_requisito.listRequisito();
        if (l_requisito.size()>0) {
            String s_res="<h2>Lista de pago</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>nombre</td>"
                    + "<td>descripcion</td>"
                    + "</tr>";
            for (Requisito i_requisito : l_requisito) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_requisito.getId()+"</td>";
                s_res=s_res+"<td>"+i_requisito.getNombre()+"</td>";
                s_res=s_res+"<td>"+i_requisito.getDescripcion()+"</td>";
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }
        private boolean validarParaListRequisito(String[] prt_parametros) {
        return true;
    }
        
    public String editRequisito(String[] prt_parametros) {
         if (validarParaEditRequisito(prt_parametros)) {
            m_requisito.setId(Integer.parseInt(prt_parametros[0]));
            m_requisito.setNombre(prt_parametros[1]);
            m_requisito.setDescripcion(prt_parametros[2]);
          
            boolean b_res=m_requisito.editRequisito();
            if (b_res) {
                return mensajeCorrecto("edicion de requisitos correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
        
    }
    private boolean validarParaEditRequisito(String[] prt_parametros) {
        return true;
    }

    
    public String delRequisito(String[] prt_parametros) {
        if (validarParaDelRequisito(prt_parametros)) {
            m_requisito.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_requisito.delRequisito();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de requisitos realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private boolean validarParaDelRequisito(String[] prt_parametros) {
        return true;
    }  
}
