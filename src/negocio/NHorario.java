/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;
import java.util.ArrayList;
import modelo.Horario;
/**
 *
 * @author josuedjh
 */
public class NHorario {
    private Horario m_horario;

    public NHorario() {
        m_horario = new Horario();
    }
    
    public String regHorario(String[] prt_parametros) {
        if (validarParaRegHorario(prt_parametros)) {
            m_horario.setDia(prt_parametros[0]);
            m_horario.setHorarioinicial(prt_parametros[1]);
            m_horario.setHorariofinal(prt_parametros[2]);
            
            boolean b_res=m_horario.regHorario();
            if (b_res) {
                return mensajeCorrecto("registro de horario correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaRegHorario(String[] prt_parametros) {
        return true;
    }
public String listHorario(String[] prt_parametros) {
        ArrayList<Horario> l_horario=new ArrayList<>();
        if (validarParaListHorario(prt_parametros)) {
            m_horario.setId(Integer.parseInt(prt_parametros[0]));
            l_horario=m_horario.listHorario();
        if (l_horario.size()>0) {
            String s_res="<h2>Lista de horario</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>Dia</td>"
                    + "<td>horario inicial</td>"
                    + "<td>horario final</td>"
                    + "</tr>";
            for (Horario i_horario : l_horario) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_horario.getId()+"</td>";
                s_res=s_res+"<td>"+i_horario.getDia()+"</td>";
                s_res=s_res+"<td>"+i_horario.getHorarioinicial()+"</td>";
                s_res=s_res+"<td>"+i_horario.getHorariofinal()+"</td>";  
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }
        private boolean validarParaListHorario(String[] prt_parametros) {
        return true;
    }
        
    public String editHorario(String[] prt_parametros) {
         if (validarParaEditHorario(prt_parametros)) {
            m_horario.setId(Integer.parseInt(prt_parametros[0]));
            m_horario.setDia(prt_parametros[1]);
            m_horario.setHorarioinicial(prt_parametros[2]);
            m_horario.setHorariofinal(prt_parametros[3]);
          
            boolean b_res=m_horario.editHorario();
            if (b_res) {
                return mensajeCorrecto("edicion de horarios correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
        
    }
    private boolean validarParaEditHorario(String[] prt_parametros) {
        return true;
    }
    
    public String delHorario(String[] prt_parametros) {
        if (validarParaDelHorario(prt_parametros)) {
            m_horario.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_horario.delHorario();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de horario realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private boolean validarParaDelHorario(String[] prt_parametros) {
        return true;
    }  
}
