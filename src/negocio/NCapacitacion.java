/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Capacitacion;

/**
 *
 * @author tecno
 */
public class NCapacitacion {
    private Capacitacion m_Capacitacion;

    public NCapacitacion() {
        m_Capacitacion=new Capacitacion();
    }

    public String regCapacitacion(String[] prt_parametros) {
        if (validarParaRegCapacitacion(prt_parametros)) {
            m_Capacitacion.setAquien(prt_parametros[0]);
            m_Capacitacion.setTitulacion(prt_parametros[1]);
            m_Capacitacion.setNombre(prt_parametros[2]);
            m_Capacitacion.setCargaHoraria(Integer.parseInt(prt_parametros[3]));
            m_Capacitacion.setInversion(Float.parseFloat(prt_parametros[4]));
            m_Capacitacion.setFechaInicio(prt_parametros[5]);
            m_Capacitacion.setTipo(Integer.parseInt(prt_parametros[6]));
            m_Capacitacion.setCategoria(Integer.parseInt(prt_parametros[7]));
            m_Capacitacion.setGestion(Integer.parseInt(prt_parametros[8]));
            
            boolean b_res=m_Capacitacion.regCapacitacion();
            if (b_res) {
                return mensajeCorrecto("registro de Capacitacion correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }

    public String listCapacitacion(String[] prt_parametros) {
        ArrayList<Capacitacion> l_Capacitacion=new ArrayList<>();
        if (validarParaListCapacitacion(prt_parametros)) {
            m_Capacitacion.setId(Integer.parseInt(prt_parametros[0]));
            l_Capacitacion=m_Capacitacion.listCapacitacion();
        if (l_Capacitacion.size()>0) {
            String s_res="<h2>Lista de Conferencias de INEGAS</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>aquien</td>"
                    + "<td>titulacion</td>"
                    + "<td>nombre</td>"
                    + "<td>carga horaria</td>"
                    + "<td>inversion</td>"
                    + "<td>fecha inicio</td>"
                    + "<td>tipo</td>"
                    + "<td>categeoria</td>"
                    + "<td>gestion</td>"
                    + "</tr>";
            for (Capacitacion i_capacitacion : l_Capacitacion) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_capacitacion.getId()+"</td>";
                s_res=s_res+"<td>"+i_capacitacion.getAquien()+"</td>";
                s_res=s_res+"<td>"+i_capacitacion.getTitulacion()+"</td>";
                s_res=s_res+"<td>"+i_capacitacion.getNombre()+"</td>";  
                s_res=s_res+"<td>"+i_capacitacion.getCargaHoraria()+"</td>";
                s_res=s_res+"<td>"+i_capacitacion.getInversion()+"</td>";  
                s_res=s_res+"<td>"+i_capacitacion.getFechaInicio()+"</td>";  
                s_res=s_res+"<td>"+i_capacitacion.getTipo()+"</td>";  
                s_res=s_res+"<td>"+i_capacitacion.getCategoria()+"</td>";  
                s_res=s_res+"<td>"+i_capacitacion.getGestion()+"</td>";  
                
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeError("parametros incorrectos");
    }

    public String editCapacitacion(String[] prt_parametros) {
        if (validarParaEditCapacitacion(prt_parametros)) {
            m_Capacitacion.setId(Integer.parseInt(prt_parametros[0]));
            m_Capacitacion.setAquien(prt_parametros[1]);
            m_Capacitacion.setTitulacion(prt_parametros[2]);
            m_Capacitacion.setNombre(prt_parametros[3]);
            m_Capacitacion.setCargaHoraria(Integer.parseInt(prt_parametros[4]));
            m_Capacitacion.setInversion(Float.parseFloat(prt_parametros[5]));
            m_Capacitacion.setFechaInicio(prt_parametros[6]);
            m_Capacitacion.setTipo(Integer.parseInt(prt_parametros[7]));
            m_Capacitacion.setCategoria(Integer.parseInt(prt_parametros[8]));
            m_Capacitacion.setGestion(Integer.parseInt(prt_parametros[9]));
            
            boolean b_res=m_Capacitacion.editCapacitacion();
            if (b_res) {
                return mensajeCorrecto("edicion de Capacitacion correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }

    public String delCapacitacion(String[] prt_parametros) {
        if (validarParaEditCapacitacion(prt_parametros)) {
            m_Capacitacion.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_Capacitacion.delCapacitacion();
            if (b_res) {
                return mensajeCorrecto("eliminacion de Capacitacion correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }

    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    

    private boolean validarParaRegCapacitacion(String[] prt_parametros) {
        return true;    
    }

    private boolean validarParaEditCapacitacion(String[] prt_parametros) {
        return true;
    }

    private boolean validarParaListCapacitacion(String[] prt_parametros) {
        return true;
    }
    
    
    
}
