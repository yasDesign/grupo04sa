/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Inscripcion;

/**
 *
 * @author tecno
 */
public class NInscripcion {
    private Inscripcion m_Inscripcion;

    public NInscripcion() {
        m_Inscripcion=new Inscripcion();
    }
    
    public String regInscripcion(String[] prt_parametros) {
        m_Inscripcion.setIdadministrador(Integer.parseInt(prt_parametros[0]));
        m_Inscripcion.setIdcapacitacion(Integer.parseInt(prt_parametros[1]));
        m_Inscripcion.setIdusuario(Integer.parseInt(prt_parametros[2]));
        m_Inscripcion.setIdpago(Integer.parseInt(prt_parametros[3]));
        boolean b_res=m_Inscripcion.regINscripcion();
        if (b_res) {
            return mensajeCorrecto("registro se realizo correctamente");
        }
        return mensajeError("registro de inscripcion incorrectos");
    }

    public String listInscripcion(String[] prt_parametros) {
        
        ArrayList<Inscripcion> l_Inscripcion=new ArrayList<>();
        if (validarParaListInscripcion(prt_parametros)) {
            m_Inscripcion.setId(Integer.parseInt(prt_parametros[0]));
            l_Inscripcion=m_Inscripcion.listInscripcion();
        if (l_Inscripcion.size()>0) {
            String s_res="<h2>Lista de Conferencias de INEGAS</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>idusuario</td>"
                    + "<td>id</td>"
                    + "<td>nombre</td>"
                    + "</tr>";
            for (Inscripcion i_inscripcion : l_Inscripcion) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_inscripcion.getId()+"</td>";
                s_res=s_res+"<td>"+i_inscripcion.getIdadministrador()+"</td>";
                s_res=s_res+"<td>"+i_inscripcion.getIdusuario()+"</td>";
                s_res=s_res+"<td>"+i_inscripcion.getIdcapacitacion()+"</td>";  
                s_res=s_res+"<td>"+i_inscripcion.getIdpago()+"</td>";  
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeError("parametros incorrectos");
    }

    public String delInscripcion(String[] prt_parametros) {
        m_Inscripcion.setId(Integer.parseInt(prt_parametros[0]));
        boolean b_res=m_Inscripcion.delInscripcion();
        if (b_res) {
           return mensajeCorrecto("eliminacion se realizo correctamente");
        }
        return mensajeError("eliminacion de inscripcion incorrectos");
    }

    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private boolean validarParaListInscripcion(String[] prt_parametros) {
        return true;
    }
    
                
}
