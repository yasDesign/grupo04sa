/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package negocio;

import java.util.ArrayList;
import modelo.Programa;

/**
 *
 * @author josuedjh
 */
public class NPrograma {
    private Programa m_programa;

    public NPrograma() {
        m_programa = new Programa();
    }
    
    public String regPrograma(String[] prt_parametros) {
        if (validarParaRegPrograma(prt_parametros)) {
            m_programa.setNombre(prt_parametros[0]);
            m_programa.setDescripcion(prt_parametros[1]);
            m_programa.setIdcapasitacion(Integer.parseInt(prt_parametros[2]));
            
            boolean b_res=m_programa.regPrograma();
            if (b_res) {
                return mensajeCorrecto("registro de Programa es correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    
    private String mensajeError(String p_parametro) {
        return  "<div class='error'><strong>ERROR!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }

    private String mensajeCorrecto(String p_parametro) {
        return  "<div class='correcto'><strong>SUCCEFULL!!! </strong><p class='texto-error'>"+p_parametro+"</p></div>";
    }
    
    private boolean validarParaRegPrograma(String[] prt_parametros) {
        return true;
    }
    
    public String listPrograma(String[] prt_parametros) {
        ArrayList<Programa> l_programa=new ArrayList<>();
        if (validarParaListPrograma(prt_parametros)) {
            m_programa.setId(Integer.parseInt(prt_parametros[0]));
            l_programa=m_programa.listPrograma();
        if (l_programa.size()>0) {
            String s_res="<h2>Lista de pago</h2>";
            s_res=s_res+"<table><tr>"
                    + "<td>id</td>"
                    + "<td>nombre</td>"
                    + "<td>descripcion</td>"
                    + "<td>Idcapasitacio</td>"
                    + "</tr>";
            for (Programa i_programa : l_programa) {
                s_res=s_res+"<tr>";
                s_res=s_res+"<td>"+i_programa.getId()+"</td>";
                s_res=s_res+"<td>"+i_programa.getNombre()+"</td>";
                s_res=s_res+"<td>"+i_programa.getDescripcion()+"</td>";
                s_res=s_res+"<td>"+i_programa.getIdcapasitacion()+"</td>";
                s_res=s_res+"</tr>";
            }
                s_res=s_res+"</table>";
            return s_res;
        }
        return mensajeCorrecto("lista vacia");
        }
        return mensajeCorrecto("parametros incorrectos");
    }
        private boolean validarParaListPrograma(String[] prt_parametros) {
        return true;
    }
        
    public String editPrograma(String[] prt_parametros) {
         if (validarParaEditPrograma(prt_parametros)) {
            m_programa.setId(Integer.parseInt(prt_parametros[0]));
            m_programa.setNombre(prt_parametros[1]);
            m_programa.setDescripcion(prt_parametros[2]);
          
            boolean b_res=m_programa.editPrograma();
            if (b_res) {
                return mensajeCorrecto("edicion de Programa correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
        
    }
    private boolean validarParaEditPrograma(String[] prt_parametros) {
        return true;
    }

    
    public String delPrograma(String[] prt_parametros) {
        if (validarParaDelPrograma(prt_parametros)) {
            m_programa.setId(Integer.parseInt(prt_parametros[0]));
            boolean b_res=m_programa.delPrograma();
            if (b_res) {
                return mensajeCorrecto("eliminiacion de Programa realizado correctamentamente");
            }
            return mensajeError("problemas con la conexion! no se pudo ejecutar la consulta ");
        }
        return mensajeError("revisar los parametros incorrectos");
    }
    private boolean validarParaDelPrograma(String[] prt_parametros) {
        return true;
    }
}
